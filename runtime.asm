section .data
	ZEROCHAR equ '0'
	NEGCHAR equ '-'
	SYS_WRITE equ 1
	SYS_READ equ 0
	STDOUT equ 1
	STDIN equ 0
	LF equ 10
	TAB equ 9
	SPACE equ 32
	NULL equ 0
	SYS_EXIT equ 60
	BUFFERLENGTH equ 20
	PROMPT: db "> ",NULL
	
section .bss
	PB: resb 21

section .text

debug:
	mov rax, '#'
	call __PRINTBYTE
	ret
	
__PRINTNL:
	mov rax, LF
	call __PRINTBYTE
	ret
	
__PRINTLONG:
	;; Prints a signed long to standard output.
	;; Arguments
	;;	rax : signed long to be printed
	;; No return values.
	;;
	push rsi
	push rbx				;
	push rcx				;
	push rdx				;
	push r11				; supernegativity flag
	push r12				; negative flag
	xor r11, r11			; clear supernegativity flag
	xor r12, r12			; clear negative flag
	mov rsi, BUFFERLENGTH	; load buffer length as offset	
	cmp rax, 0				; is it a positive value?
	jnl __printlong_loop	;   ...ja: continue to main loop
	mov r12, 1				; set negative flag
	neg rax					; supernegativity check:
	cmp rax, 0				;   is it still negative?
	jnl __printlong_loop	;     ...no: continue to main loop
	add rax, 1				;   exit supernegativity by adding one
	neg rax					;   negate it again
	mov r11, 1				;   set supernegativity flag
__printlong_loop:
	cmp rax, 0				; have we reached zero?
	je __printlong_stop		;   ...ja: then we're done
	mov rdx, 0				; prepare division by clearing rdx
	mov rcx, qword 10		; we divide by 10
	idiv rcx				; perform division
	add rdx, qword ZEROCHAR	; add '0' to the remainder to get digit character
	test r11, r11			; supernegativity?
	jz __printlong_write	;   ...no: continue to write character to buffer
	mov r11, 0				; clear supernegativity flag
	add rdx, 1				; add one to the first digit character
__printlong_write:
	mov [PB+rsi], dl		; write character to buffer at offset
	dec rsi					; decrease offset as we move from back to front
	jmp __printlong_loop	; continue with main loop
__printlong_stop:
	cmp rsi, BUFFERLENGTH	; did we write anything? happens when writing zero
	jne __printlong_negative;   ...ja: continue to write negative sign
	mov byte [PB+rsi], ZEROCHAR	; write zero character
	dec rsi					; decrement offset
__printlong_negative:
	test r12, r12			; is negative flag set?
	jz __printlong_positive	;   ...no: continue to offset adjustment
	mov byte [PB+rsi], NEGCHAR	; write minus sign character
	jmp __printlong_printer	; continue to printer
__printlong_positive:
	inc rsi					; increment offset since it points too far here
__printlong_printer:
	mov rax, SYS_WRITE
	mov rdi, STDOUT
	; Calculate length
	mov rdx, BUFFERLENGTH
	sub rdx, rsi
	add rdx, 1
	; Offset into buffer
	mov rbx, PB
	add rsi, rbx
	syscall
	pop r12
	pop r11
	pop rdx
	pop rcx
	pop rbx
	pop rsi
	ret
	
__PRINTBYTE:
	;; Prints a byte to standard output.
	;; Arguments:
	;;  rax: the byte to print
	;; No return values.
	;;
	push rsi
	push rdx
	push rdi
	push rax
	mov rsi, rsp
	mov rdx, 1
	mov rax, SYS_WRITE
	mov rdi, STDOUT
	syscall
	pop rax
	pop rdi
	pop rdx
	pop rsi
	ret	

__PRINTTEXT:
	;; Prints a C style text string.
	;; Arguments:
	;;  rax: pointer to the string
	;; No return values.
	;;
	push rbx
	mov rbx, rax
	xor rax, rax
__printtext_loop:
	mov al, byte [rbx]
	cmp rax, NULL
	je __printtext_done
	call __PRINTCHAR
	inc rbx
	jmp __printtext_loop
__printtext_done:
	pop rbx
	ret

__PRINTCHAR:
	;; Prints a visible character to standard output.
	;; Only newline (LF) and tab is allowed below space (' ').
	;; Arguments:
	;;  rax: the character to print
	;; No return values.
	;;
	cmp rax, SPACE
	jl __printchar_unprintable
	cmp rax, 128
	jl __printchar_print
__printchar_unprintable:
	cmp rax, LF
	je __printchar_print
	cmp rax, TAB
	je __printchar_print
	mov rax, 63
__printchar_print:
	call __PRINTBYTE
	ret

__SKIPLINE:
	;; Reads from standard input until it sees LF.
	;; No arguments.
	;; No return values.
	;;
	call __READBYTE
	cmp rax, LF
	jne __SKIPLINE
	ret
	
__READBYTE:
	;; Reads any byte from standard input.
	;; No arguments.
	;; Return values:
	;;  rax: the byte
	;;
	push rsi
	push rdi
	push rdx
	push 0
	mov rsi, rsp
	mov rdi, STDIN
	mov rdx, 1
	mov rax, SYS_READ
	syscall
	pop rax					; pops the return value
	pop rdx
	pop rdi
	pop rsi
	ret
	
__READCHAR:
	;; Reads a character from standard input, but ignores LF.
	;; No arguments.
	;; Return values:
	;;  rax: the character byte
	;;
	call __READBYTE
	cmp rax, LF
	je __READCHAR
	ret

__READSOME:
	;; Reads bytes from standard input until either the buffer is
	;; filled up, or the LF character is encountered.
	;; Arguments:
	;;	rax: pointer to buffer
	;;  rbx: buffer length
	;;  rcx: 0 = ignore empty lines, 1 = accept empty with only LF in buffer
	;; Return values:
	;;  rax: number of bytes read into buffer
	;;  rbx: 0 = no LF in buffer, 1 = LF last character in buffer
	;;
	push r12
	push r13
	push r14
	push r15
	mov r12, rax			; pointer to buffer
	mov r13, 0				; offset
	mov r14, rcx			; LF control flag
	mov r15, rbx			; buffer length
	mov rbx, 0				; clear return value rbx
	cmp r15, 0				; is buffer length zero or negative?
	jle __readsome_done		;   ...ja: then we're done
__readsome_first:
	call __READBYTE			; read a byte, return value in rax
	cmp rax, LF				; is it LF?
	jne __readsome_loop		;   ...no: then jump to main read loop
	cmp r14, 1				;   ...ja: do we accept it?
	je __readsome_lf		;     ...ja: then we're done
	jmp __readsome_first	;     ...no: retry reading a byte
__readsome_loop:
	mov byte [r12+r13], al	; write byte to buffer at offset
	inc r13					; increment offset
	cmp r13, r15			; is offset equal to buffer length?
	je __readsome_done		;   ...ja: then we're done
	call __READBYTE			; read next byte
	cmp rax, LF				; is it LF?
	je __readsome_lf		;   ...ja: then we're done
	jmp __readsome_loop		;   ...no: loop around
__readsome_lf:
	mov byte [r12+r13], LF	; write the LF to the buffer
	mov rbx, 1				; set return LF flag in rbx
	inc r13					; increment offset
__readsome_done:
	mov rax, r13			; move offset to return register rax
	pop r15
	pop r14
	pop r13
	pop r12
	ret

__READLONG:
	;; Reads a signed long from standard input.
	;; No arguments.
	;; Return values:
	;;	rax: signed long result, if successful
	;;	rbx: 0 = success, 1 = error
	;;
	push r12				; read bytes
	push r13				; offset into buffer
	push r14				; accumulator
	push r15				; negative flag	
	mov rax, PB				; arg 1 = ptr to buffer
	mov rbx, BUFFERLENGTH	; arg 2 = buffer length
	mov rcx, 0 				; arg 3 = discard empty lines
	call __READSOME			; return values in rax and rbx
	cmp rax, 0 				; did we read zero bytes?
	je __readlong_error		;   ...no: abort
	mov r12, rax			; move read bytes to r12
	mov r13, 0				; start at offset zero
	mov r14, 0				; set accumulator to zero
	mov r15, 0				; negative flag unset
	sub r12, rbx			; subtract terminating LF from read bytes if present
	cmp rbx, 1				; LF present?
	je __readlong_check		;   ...ja: continue
	call __SKIPLINE			;   ...no: discard the line upto LF
__readlong_check:
	mov rbx, 0				; clear error flag
	mov al, byte [PB]		; fetch first byte
	cmp al, '+'				; is it a plus sign?
	je __readlong_clear		;   ...ja: ignore it
	cmp al, '-'				; is it a minus sign?
	jne __readlong_loop		;   ...no: continue to main read loop
	mov r15, 1				;   ...ja: set negative flag
__readlong_clear:
	inc r13					; increment offset to skip sign character
__readlong_loop:
	xor rax, rax			; clear rax
	mov al, byte [PB+r13]	; fetch offset byte
	cmp rax, byte '0'		
	jl __readlong_error		; if rax < '0' then abort
	cmp rax, byte '9'
	jg __readlong_error		; if rax > '9' then abort
	sub rax, '0'			; subtract '0' from the digit character
	imul r14, 10			; multiply accumulator with 10
	add r14, rax			; add the digit to the accumulator
	cmp r14, 0				; check for negativity:
	jnl __readlong_continue	;   if not negative, continue
	cmp r15, 1				;   is negative flag set?
	jne __readlong_error	;     ...no: abort
	neg r14					;   only supernegativity is allowed at this stage
	cmp r14, 0				;   is it still less than zero?
	jnl __readlong_error	;     ...no: abort
__readlong_continue:
	inc r13					; increment offset
	cmp r13, r12			; is offset equal to read bytes?
	jne __readlong_loop		;   ...no: continue with main read loop
	cmp r15, 1				; do we have a minus sign?
	jne __readlong_prologue	;   ...no: jump to prologue
	neg r14					;   ...ja: negate the number
__readlong_prologue:
	mov rax, r14			; move number to return register rax
	pop r15
	pop r14
	pop r13
	pop r12
	ret
__readlong_error:
	mov rbx, 1				; set error return value in rbx
	jmp __readlong_prologue	; jump to prologue

__RETRYREADLONG:
	;; Read from standard input until it successfully
	;; reads a signed long value.
	;; Arguments:
	;;  rax: pointer to a c style string for the prompt. 0 = none
	;; Return values:
	;;  rax: the signed long value
	push r12
	mov r12, rax
__retryreadlong_loop:
	cmp r12, 0
	je __retryreadlong_read
	mov rax, r12
	call __PRINTTEXT
__retryreadlong_read:
	call __READLONG
	cmp rbx, 0
	jne __retryreadlong_loop
	pop r12
	ret

__PRINTLONGARRAY:
	;; Prints an array of signed longs.
	;; Arguments:
	;;  rax: pointer to array
	;;  rbx: 
	
