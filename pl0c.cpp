/*
 * Copyright: Parasite Network
 * AGPL 3
 */

/*
 * PL/0 compiler.
 *
 * program	= block "." .
 * block	= [ "const" ident "=" number { "," ident "=" number } ";" ]
 *		  [ "var" ident [ ":" number ] { "," ident [ ": " number ] } ";" ]
 *		  { "procedure" ident ";" block ";" } statement .
 * statement	= [ 
 *			"printi" expression
 *		  |	"printc" expression
 *		  | ident ":=" expression
 *		  | "call" ident
 *		  | "begin" statement { ";" statement } "end"
 *		  | "if" condition "then" statement
 *		  | "while" condition "do" statement
 *		  | "readInt" [ "into" ] ident
 *		  | "writeInt" ( ident | number )
 *		  | "readChar" [ "into" ] ident
 *		  | "writeChar" ( ident | number) ] .
 * condition	= "odd" expression
 *		| expression ( "=" | "#" | "<" | ">" ) expression .
 * expression	= [ "+" | "-" ] term { ( "+" | "-" ) term } .
 * term		= factor { ( "*" | "/" ) factor } .
 * factor	= ident
 *		| number
 *		| "geti" 
 *		| "getc"
 *		| "(" expression ")" .
 * number = digits | char-constant
 */

#include <vector>
#include <string>
#include <fstream>
#include <iterator>
#include <algorithm>
#include <iostream>
#include <cctype>
#include <sstream>
#include <map>

char const * NasmCommand = "nasm -f elf64 -l -g -F stabs $.asm";
char const * LdCommand = "ld -s -g -o $ $.o";

std::string command(char const * cmd, std::string const & sub)
{
	std::string output;
	while(*cmd)
	{
		if(*cmd == '$')
		{
			output += sub;
		}
		else
		{
			output += *cmd;
		}
		++cmd;
	}
	return output;
}

bool system(char const * cmd, std::string const & sub)
{
	return std::system(command(cmd, sub).c_str()) == 0;
}

/*----------------------------------------------------------------------------*/

struct Sourcecode
{
	std::vector<char> code;
	bool open(std::string const & file)
	{
		code.clear();
		std::ifstream in(file, std::ios_base::binary|std::ios_base::in);
		if(!in)
		{
			code.push_back(0);
			return false;
		}
		std::istreambuf_iterator<char> begin{in}, end;
		std::copy(begin, end, std::back_inserter(code));
		code.push_back(0);
		return true;
	}
	char const * begin() const
	{
		return &code[0];
	}
	char const * end() const
	{
		return &code[code.size() - 1];
	}
};

/*----------------------------------------------------------------------------*/

struct AnyResult
{
	bool const accepted;
	char const character;
	operator bool() const
	{
		return accepted;
	}
};

/*----------------------------------------------------------------------------*/

struct SyntaxErrorException
{

};

/*----------------------------------------------------------------------------*/

struct String
{
	char const * begin;
	char const * end;
	int row;
	std::string to_string() const
	{
		return std::string{begin, end};
	}
	unsigned long length() const
	{
		return end - begin;
	}
};

std::ostream & operator<<(std::ostream & out, String const & s)
{
	char const * ptr = s.begin;
	while(ptr != s.end)
	{
		out << *ptr;
		++ptr;
	}
	return out;
}

String stringify(char const * text)
{
	String s{text, nullptr, -1};
	while(*text)
	{
		++text;
	}
	s.end = text;
	return s;
}

/*----------------------------------------------------------------------------*/

struct Long
{
	String where;
	long value;
};

/*----------------------------------------------------------------------------*/

struct Iterator
{
	char const * begin;
	char const * current;
	char const * end;
	int row;
	
	bool good()
	{
		ignore();
		return current != end;
	}
	
	Iterator(Sourcecode const & sc)
	{
		begin = current = sc.begin();
		end = sc.end();
		row = 1;
	}
	
	void ignore()
	{
		while(current != end && (*current <= ' ' || *current == '{'))
		{
			if(*current == '{')
			{
				char const * current_backup = current;
				int row_backup = row;
				while(current != end && *current != '}')
				{
					next();
				}
				if(*current != '}')
				{
					current = current_backup;
					row = row_backup;
					error_expectation("Missing terminating '}'.");
				}
			}
			next();
		}
	}
	
	bool accept(char c)
	{
		ignore();
		if(*current == c)
		{
			next();
			return true;
		}
		return false;
	}
	
	void expect(char c)
	{
		if(!accept(c))
		{
			char const m[4] = {'\'', c, '\'', 0};
			error_expectation(m);
		}
	}
	
	AnyResult any(char const * anyptr)
	{
		ignore();
		while(*anyptr)
		{
			if(accept(*anyptr))
			{
				return {true, *anyptr};
			}
			++anyptr;
		}
		return {false, *current};		
	}
	
	AnyResult expect_any(char const * anyptr)
	{
		AnyResult const result = any(anyptr);
		if(result)
		{
			return result;
		}
		
		std::string list = "Any of ";
		while(*anyptr)
		{
			list += "'";
			list += *anyptr;
			list += "'";
			if(*(anyptr + 1))
			{
				if(*(anyptr + 2))
				{
					list += ", ";
				}
				else
				{
					list += " or ";	
				}
			}
			++anyptr;
		}
		
		error_expectation(list);
	}
	
	void next()
	{
		if(current != end)
		{
			if(*current == '\n')
			{
				++row;
			}
			++current;
		}
	}
	
	bool accept(char const * text)
	{
		ignore();
		char const * temp = current;
		while((temp != end) && *text)
		{
			if(*temp != *text)
			{
				return false;
			}
			++temp;
			++text;
		}
		if(!*text)
		{
			current = temp;
			return true;
		}
		return false;
	}
	
	bool expect(char const * text)
	{
		if(!accept(text))
		{
			error_expectation(text);
		}
		return true;
	}
	
	bool accept_id(String & ident)
	{
		ignore();
		
		if(!std::isalpha((unsigned char)*current))
		{
			return false;
		}
		
		ident.row = row;
		ident.begin = current;
		
		do
		{
			++current;
		} while(std::isalnum((unsigned char)*current));
		
		ident.end = current;
		
		return true;
	}
	
	String expect_id()
	{
		String id;
		if(!accept_id(id))
		{
			error_expectation("Identifier");
		}
		return id;
	}
	
	bool accept(Long & num)
	{
		ignore();
		
		Iterator backup{*this};
		
		if(accept('\''))
		{
			if(*current < ' ' || *current > 127)
			{
				*this = backup;
				return false;
			}
			num.value = *current;
			num.where.row = row;
			num.where.begin = current;
			num.where.end = current + 1;
			++current;
			if(!accept('\''))
			{
				*this = backup;
				return false;
			}
			return true;
		}
		
		num.value = 0;
		num.where.row = row;
		num.where.begin = current;
		if(!std::isdigit((unsigned char)*current))
		{
			return false;
		}
		
		do
		{
			num.value *= 10;
			num.value += *current - '0';
			++current;
		} while(std::isdigit((unsigned char)*current));
		
		num.where.end = current;
		
		return true;
	}
	
	bool accept_text(String & text)
	{
		ignore();
		if(*current != '"')
		{
			return false;
		}
		text.row = row;
		Iterator backup{*this};
		++current;
		text.begin = current;
		while(std::isalnum((unsigned char)*current))
		{
			++current;
		}
		if(*current != '"')
		{
			*this = backup;
			return false;
		}
		text.end = current;
		++current;
		return true;
	}
	
	Long expect_num()
	{
		Long num;
		if(!accept(num))
		{
			error_expectation("Number");
		}
		return num;
	}
	
	char const * zuryck(char const * beginning, char const * ptr) const
	{
		while(ptr != beginning && *(ptr - 1) != '\n')
		{
			--ptr;
		}
		return ptr;
	}
	
	void write_error_message(std::string const & message, int row) const
	{
		std::cout << "Syntax error at row " << row << ": " << message << "\n\n";
	}
	
	void write_error_row(char const * start, char const * where) const
	{
		std::string::size_type spaces = 0;
		std::string::size_type tabs = 0;
		while(*start != '\n')
		{
			if(start < where)
			{
				if(*start == '\t')
				{
					++tabs;
				}
				else
				{
					++spaces;
				}
			}
			std::cout << *start;
			++start;
		}
		std::cout << "\n";
		std::cout << std::string(spaces, (char)' ');
		std::cout << std::string(tabs, (char)'\t');
	}
	
	void error_identifier(String ident, std::string const & message) const
	{
		char const * ptr = ident.begin;
		char const * start = zuryck(begin, ptr);
		write_error_message(message, ident.row);
		write_error_row(start, ptr);
		while(ptr != ident.end)
		{
			std::cout << "~";
			++ptr;
		}
		std::cout << "\n";
		throw SyntaxErrorException{};
	}
	
	void error_expectation(std::string const & message) const
	{
		char const * start = zuryck(begin, current);
		std::string const msgprefix = "Expected: ";
		write_error_message(msgprefix + message, row);
		write_error_row(start, current);
		std::cout << "^\n";
		throw SyntaxErrorException{};
	}
	
	void error_syntax() const
	{
		char const * start = zuryck(begin, current);
		write_error_message("Syntax error", row);
		write_error_row(start, current);
		std::cout << "^\n";
		throw SyntaxErrorException{};
	}
};

/*----------------------------------------------------------------------------*/

enum class Symboltype
{
	Constant,
	Variable,
	Function,
	Array,
};

/*----------------------------------------------------------------------------*/

enum class Arraytype
{
	Long,
	Char,
};

/*----------------------------------------------------------------------------*/

struct ArrayInfo
{
	long size;
	Arraytype type;
};

/*----------------------------------------------------------------------------*/

struct SymbolInfo
{
	Symboltype type;
	ArrayInfo arrayinfo;
	
	bool is_assignable() const
	{
		return type == Symboltype::Variable || type == Symboltype::Array;
	}
};

/*----------------------------------------------------------------------------*/

struct FindResult
{
	bool found;
	std::string truename;
	SymbolInfo info;
};

/*----------------------------------------------------------------------------*/

struct AddResult
{
	bool ok;
	std::string truename;
};

/*----------------------------------------------------------------------------*/

struct Environment
{
	std::ostringstream asmout;
	
	int level = -1;
	unsigned long nestidx = 0;	
	std::string nestingsuffix;
	char const nestingdelimiter = '~';
	std::string const prefix = "X~";
	std::map<std::string, SymbolInfo> symbols;
	
	std::string get_label()
	{
		static long counter = 0;
		return std::string("L") + std::to_string(counter++);
	}
	
	std::string get_label(std::string const & suffix)
	{
		std::string const & label = get_label();
		return label + "_" + suffix;
	}
	
	std::string besmycken(String ident) const
	{
		std::string const truename = prefix + ident.to_string() + nestingsuffix;
		return truename;
	}
	
	AddResult add_symbol(String ident, Symboltype type)
	{
		std::string const truename = besmycken(ident);
		
		SymbolInfo si;
		si.type = type;
		
		if(symbols.insert(std::make_pair(truename, si)).second)
		{
			return {true, truename};
		}
		return {false};		
	}
	
	void upgrade_to_array(String ident, ArrayInfo ai)
	{
		std::string const truename = besmycken(ident);
		
		auto pos = symbols.find(truename);
		if(pos != symbols.end())
		{
			(*pos).second.arrayinfo = ai;
			(*pos).second.type = Symboltype::Array;
		}
	} 
	
	std::string dropsuffix(std::string const & nestedsymbol) const
	{
		std::string::size_type const at = nestedsymbol.rfind(nestingdelimiter);
		return nestedsymbol.substr(0, at);
	}
	
	FindResult findit(std::string const & nestedsymbol, int level) const
	{
		auto pos = symbols.find(nestedsymbol);
		if(pos == symbols.end())
		{
			if(level > 0)
			{
				std::string const upsymbol = dropsuffix(nestedsymbol);
				return findit(upsymbol, level - 1);
			}
			return {false};
		}
		return {true, nestedsymbol, (*pos).second};
	}
	
	FindResult find(String ident) const
	{
		std::string const truename = besmycken(ident);
		return findit(truename, level); 
	}
	
	bool is_const(std::string const & symbol) const
	{
		auto pos = symbols.find(symbol);
		if(pos == symbols.end())
		{
			return false;
		}
		return (*pos).second.type == Symboltype::Constant;
	}
	
	bool toplevel() const
	{
		return level == 0;
	}
	
	void enter()
	{
		if(++level)
		{
			nestingsuffix.push_back(nestingdelimiter);
			nestingsuffix += std::to_string(nestidx++);
		}
	}
	
	void leave()
	{
		if(level--)
		{
			nestingsuffix = dropsuffix(nestingsuffix);
		}
	}
};

/*----------------------------------------------------------------------------*/

void parse_expression(Iterator & input, Environment & env);

/*----------------------------------------------------------------------------*/

void parse_factor(Iterator & input, Environment & env)
{
	if(input.accept("geti"))
	{
		env.asmout << "\tmov rax, PROMPT\n";
		env.asmout << "\tcall __RETRYREADLONG\n";
		return;
	}
	
	if(input.accept("geti"))
	{
		env.asmout << "\tmov rax, PROMPT\n";
		env.asmout << "\tcall __PRINTTEXT\n";
		env.asmout << "\tcall __READCHAR\n";
		return;
	}
	
	if(input.accept("sizeof"))
	{
		String ident = input.expect_id();
		
		FindResult const result = env.find(ident);
		
		if(!result.found)
		{
			input.error_identifier(ident, "Unknown");
		}
		
		if(result.info.type != Symboltype::Array)
		{
			input.error_identifier(ident, "Not an array");
		}
		
		env.asmout << "\tmov rax, " << result.info.arrayinfo.size << "\n";
		return;
	}	

	String ident;
	if(input.accept_id(ident))
	{
		FindResult const result = env.find(ident);
		
		if(!result.found)
		{
			input.error_identifier(ident, "Unknown");
		}
	
		if(result.info.type == Symboltype::Constant)
		{
			env.asmout << "\tmov rax, " << result.truename << "\n";
		}
		else if(result.info.type == Symboltype::Variable)
		{
			env.asmout << "\tmov rax, qword [" << result.truename << "]\n";
		}
		else if(result.info.type == Symboltype::Array)
		{
			input.expect('[');
			
			parse_expression(input, env);
			
			input.expect(']');
			
			env.asmout << "\tadd rax, " << result.truename << "\n";
			
			switch(result.info.arrayinfo.type)
			{
				case Arraytype::Long:
					env.asmout << "\tmov rax, [rax]\n";
					break;
				case Arraytype::Char:
					env.asmout << "\tmov al, byte [rax]\n";
					env.asmout << "\tand rax, 0xFF\n";
					break;
				default:
					__builtin_unreachable();
			}

			return;
		}
		else
		{
			input.error_identifier(ident, "Misused identifier");
		}
		return;
	}
	
	Long num;
	if(input.accept(num))
	{
		env.asmout << "\tmov rax, " << num.value << "\n";
		return;
	}
	
	if(input.accept("("))
	{
		env.asmout << "\tpush rbx\n";
		parse_expression(input, env);
		env.asmout << "\tpop rbx\n";
		input.expect(")");
		return;
	}
	
	input.error_syntax();
}

/*----------------------------------------------------------------------------*/

void parse_term(Iterator & input, Environment & env)
{
	parse_factor(input, env);
		
	while(AnyResult const op = input.any("*/"))
	{
		env.asmout << "\tmov rbx, rax\n";

		parse_factor(input, env);

		switch(op.character)
		{
			case '*':
				env.asmout << "\timul rbx\n";
				break;
			case '/':
			{
				env.asmout << "\txchg rax, rbx\n";
				env.asmout << "\tmov rdx, 0\n";
				env.asmout << "\tidiv rbx\n";
				break;
			}
			default:
				__builtin_unreachable();
		}
	}
}

/*----------------------------------------------------------------------------*/
/*
 * Return value in rax.
 */
void parse_expression(Iterator & input, Environment & env)
{
	AnyResult const sign = input.any("+-");
	
	parse_term(input, env);
	
	if(sign.character == '-')
	{
		env.asmout << "\tneg rax\n";
	}
	
	while(AnyResult const op = input.any("+-"))
	{
		env.asmout << "\tmov rbx, rax\n";
	
		parse_term(input, env);
		
		switch(op.character)
		{
			case '+':
				env.asmout << "\tadd rax, rbx\n";
				break;
			case '-':
				env.asmout << "\tmov rdx, rax\n";
				env.asmout << "\tmov rax, rbx\n";
				env.asmout << "\tsub rax, rdx\n";
				break;
			default:
				__builtin_unreachable();
		}
	}
}

/*----------------------------------------------------------------------------*/

void parse_condition(Iterator & input, Environment & env)
{
	if(input.accept("odd"))
	{
		parse_expression(input, env);
		env.asmout << "\tand rax, 1\n";
		return;
	}
	
	parse_expression(input, env);
	
	AnyResult const op = input.expect_any("=#<>");
	
	env.asmout << "\tmov rbx, rax\n";
	
	parse_expression(input, env);

	env.asmout << "\tcmp rbx, rax\n";
	env.asmout << "\tmov rax, 1\n";
	
	std::string const & label = env.get_label();
	
	switch(op.character)
	{
		case '=':
			env.asmout << "\tje " << label << "\n";
			break;
		case '#':
			env.asmout << "\tjne " << label << "\n";
			break;
		case '<':
			env.asmout << "\tjl " << label << "\n";
			break;
		case '>':
			env.asmout << "\tjg " << label << "\n";
			break;			
	}
	
	env.asmout << "\tmov rax, 0\n";
	env.asmout << label << ":\n";
}

/*----------------------------------------------------------------------------*/

void parse_call(Iterator & input, Environment & env)
{
	String ident = input.expect_id();
	
	FindResult const result = env.find(ident);
	
	if(!result.found)
	{
		input.error_identifier(ident, "Unknown");
	}
	
	if(result.info.type != Symboltype::Function)
	{
		input.error_identifier(ident, "Not a procedure name");
	}
	env.asmout << "\tcall " << result.truename << "\n";
}

/*----------------------------------------------------------------------------*/

void parse_statement(Iterator & input, Environment & env)
{
	if(input.accept("call"))
	{
		parse_call(input, env);
		return;
	}
	
	if(input.accept("begin"))
	{
		do
		{
			parse_statement(input, env);
		} 
		while(input.accept(';'));
		
		input.expect("end");
		return;
	}
	
	if(input.accept("if"))
	{
		parse_condition(input, env);
		input.expect("then");
		std::string const & label = env.get_label();
		env.asmout << "\tcmp rax, 0\n";
		env.asmout << "\tjz " << label << "\n";
		parse_statement(input, env);
		env.asmout << label << ":\n";
		return;
	}
	
	if(input.accept("while"))
	{
		std::string const & start = env.get_label("whilestart");
		env.asmout << start << ":\n";
		parse_condition(input, env);
		input.expect("do");
		std::string const & label = env.get_label("whileend");
		env.asmout << "\tcmp rax, 0\n";
		env.asmout << "\tjz " << label << "\n";
		parse_statement(input, env);
		env.asmout << "\tjmp " << start << "\n";
		env.asmout << label << ":\n";
		return;
	}

	if(input.accept("printi"))
	{
		parse_expression(input, env);
		env.asmout << "\tcall __PRINTLONG\n";
		return;
	}
	
	if(input.accept("printc"))
	{
		parse_expression(input, env);
		env.asmout << "\tcall __PRINTCHAR\n";
		return;
	}
	
	if(input.accept("nl"))
	{
		env.asmout << "\tcall __PRINTNL\n";
		return;
	}

	String ident;
	if(input.accept_id(ident))
	{
		FindResult const result = env.find(ident);
		
		if(!result.found)
		{
			input.error_identifier(ident, "Unknown");
		}
	
		if(!result.info.is_assignable())
		{
			input.error_identifier(ident, "Not assignable");
		}
	
		if(result.info.type == Symboltype::Array)
		{
			input.expect('[');
			
			parse_expression(input, env);
			
			input.expect(']');
			
			env.asmout << "\tpush rax\n";
		}
		
		input.expect(":=");
		parse_expression(input, env);
	
		if(result.info.type == Symboltype::Variable)
		{
			env.asmout << "\tmov [" << result.truename << "], rax\n";
		}
		
		if(result.info.type == Symboltype::Array)
		{
			env.asmout << "\tpop rbx\n";
			env.asmout << "\tadd rbx, " << result.truename << "\n";
			switch(result.info.arrayinfo.type)
			{
				case Arraytype::Long:
					env.asmout << "\tmov [rbx], rax\n";
					break;
				case Arraytype::Char:
					env.asmout << "\tmov byte [rbx], al\n";
					break;
				default:
					__builtin_unreachable();
			}
		}
	}
}

/*----------------------------------------------------------------------------*/

void parse_var(Iterator & input, Environment & env)
{
	int sectionid = 0;
	char const * sectionheader[2] = {
		"section .bss\n",
		"section .data\n",
	};
	
	do
	{
		String ident = input.expect_id();
		
		AddResult const result = env.add_symbol(ident, Symboltype::Variable);
	
		if(!result.ok)
		{
			input.error_identifier(ident, "Duplicate");
		}
		
		if(input.accept(':'))
		{
			std::cout << "A!\n";
			if(sectionid != 1)
			{
				sectionid = 1;
				env.asmout << sectionheader[1];
			}
		
			ArrayInfo ai;
		
			String text;
			if(input.accept_text(text))
			{				
				ai.size = text.length();
				ai.type = Arraytype::Char;
				env.asmout << "\t" << result.truename << ": db \"" << text << "\",NULL\n";
			}
			else
			{
				std::cout << "L!\n";
				Long num = input.expect_num();
				if(num.value < 1)
				{
					input.error_identifier(num.where, "Array size error");
				}
				ai.size = num.value;
				ai.type = Arraytype::Long;
				
				env.asmout << "\t" << result.truename << ": times " << num.value << " dq (0)\n";
			}
			
			env.upgrade_to_array(ident, ai);
		}
		else
		{
			if(sectionid != 0)
			{
				sectionid = 0;
				env.asmout << sectionheader[0];
			}
			env.asmout << "\t" << result.truename << ": resq 1\n";
		}
		
	} 
	while(input.accept(","));
	
	input.expect(';');
}

/*----------------------------------------------------------------------------*/

void parse_const(Iterator & input, Environment & env)
{
	do
	{
		String ident = input.expect_id();

		AddResult const result = env.add_symbol(ident, Symboltype::Constant);
	
		if(!result.ok)
		{
			input.error_identifier(ident, "Duplicate");
		}
		
		input.expect('=');
		Long num = input.expect_num();
		
		env.asmout << "\t" << result.truename << " equ " << num.value << "\n";
	} 
	while(input.accept(','));
	
	input.expect(';');
}

/*----------------------------------------------------------------------------*/

void parse_block(Iterator & input, Environment & env, std::string const & entry);

/*----------------------------------------------------------------------------*/

void parse_procedure(Iterator & input, Environment & env)
{
	String ident = input.expect_id();
	
	AddResult const result = env.add_symbol(ident, Symboltype::Function);
	
	if(!result.ok)
	{
		input.error_identifier(ident, "Duplicate");
	}
	
	input.expect(";");
	
	parse_block(input, env, result.truename);
	input.expect(";");
	
	env.asmout << "\tret\n";
}

/*----------------------------------------------------------------------------*/

void parse_block(Iterator & input, Environment & env, std::string const & entry)
{
	env.enter();

	env.asmout << "\nsection .data\n";

	if(input.accept("const"))
	{
		parse_const(input, env);
	}
	
	env.asmout << "\nsection .bss\n";
	
	if(input.accept("var"))
	{
		parse_var(input, env);
	}

	env.asmout << "\nsection .text\n";
	
	while(input.accept("procedure"))
	{
		parse_procedure(input, env);
	}

	env.asmout << "\n;\n;\n\n";
	env.asmout << entry << ":\n";

	parse_statement(input, env);
	
	env.leave();
}

/*----------------------------------------------------------------------------*/

void parse_program(Iterator & input, Environment & env)
{
	input.ignore();

	env.asmout << "; PL/0 ASSEMBLY\n";
	env.asmout << ";;\n\n";
	
	env.asmout << "%include \"runtime.asm\"\n";
	
	env.asmout << "\nsection .data\n";
	env.asmout << "\tglobal _start\n";
	
	parse_block(input, env, "_start");
	
	input.expect(".");
	
	env.asmout << "\tmov rax, SYS_EXIT\n";
	env.asmout << "\tmov rdi, 0\n";
	env.asmout << "\tsyscall\n";
	
	input.ignore();
}

/*----------------------------------------------------------------------------*/

int main(int argc, char ** argv)
{
	if(argc < 2)
	{
		std::cout << "Missing file name\n";
		return 1;
	}

	std::string const & file = argv[1];
	
	std::string const & suffix = ".pl0";
	
	if(file.size() < 5 || !std::equal(suffix.rbegin(), suffix.rend(), file.rbegin()))
	{
		std::cout << "The file must end with '.pl0'\n";
		return 1;
	}

	std::string::size_type const suffixdot = file.rfind('.');
	std::string const & filename = file.substr(0, suffixdot);

	Sourcecode sc;
	if(!sc.open(file))
	{
		std::cout << "Failed to open '" << file << "'\n";
		return 1;
	}
	
	Iterator ii{sc};
	
	Environment env;
	
	try
	{
		parse_program(ii, env);
		if(ii.good())
		{
			ii.error_expectation("Unexpected noise.");
		}
	}
	catch(SyntaxErrorException const & e)
	{
		return 1;
	}
	
	if(argc == 3 && argv[2][0] == 'c')
	{
		std::string const & asmfile = filename + ".asm";
		std::ofstream out(asmfile);
		
		if(out)
		{
			out << env.asmout.str();
			out.close();

			if(!system(NasmCommand, filename))
			{
				std::cout << "nasm failed!\n";
				return 1;
			}
			
			if(!system(LdCommand, filename))
			{
				std::cout << "ld failed!\n";
				return 1;
			}
		}
	}

	return 0;
}
